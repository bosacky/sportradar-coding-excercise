val scala3Version = "3.2.2"

lazy val root = project
  .in(file("."))
  .settings(
    name := "sportradar-coding-exercise",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "org.typelevel" %% "discipline-scalatest" % "2.2.0",
      "org.typelevel" %% "cats-core" % "2.9.0",
      "org.typelevel" %% "discipline-core" % "1.5.1",
      "org.scalactic" %% "scalactic" % "3.2.15",
      "org.typelevel" %% "cats-effect" % "3.4.8",
      "org.typelevel" %% "cats-laws" % "2.9.0" % Test,
      "org.scalacheck" %% "scalacheck" % "1.17.0" % Test,
      "org.scalatest" %% "scalatest" % "3.2.15" % Test,
      "org.scalameta" %% "munit" % "0.7.29" % Test
    )
  )
