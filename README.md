## Sportradar Coding Exercise

### Motive

This is a functional approach on Sportradar Coding Exercise.
I decided for fully functional approach, no mutation, no side effects.
The score board state is handled via the state monad.

#### Structures

- `Score` represents the game score, it has two members `away` and `home` representing corresponding team score. The
  companion object provides `empty` instance with zero:zero score.
- `PlayingTeams` case class representing the playing teams, it provides two methods to verify if a team (or teams) are
  involved in that game
- given/implicit ordering for tuple `(PlayingTeams, Score)` is used to sort the summary table
- `ScoreBoardState` the state of the game consisting of `ListMap` with keys of `PlayingTeams` and values of `Score`.
  This allows easy access to the specific game for an score update while keeps entries in the insertion order needed for
  the sorted summary.
- `ScoreBoardOp` state monad representing state of `ScoreBoardState`
- `ScoreBoard` the main object representing the scoreboard with methods to `startGame`, `updateScore`, `finishGame`
  and `getSummary` returning a `ScoreBoardOp` monad for each of the operations.

#### Implementation logic

As already mentioned `ListMap` is used to handle the ongoing games which allows to keep the games in insertion order
while providing easy of lookup.
In this case the ease of lookup does not mean optimal performance, I focused on the simplicity of use not considering
performance much.
If the table was huge and the requirement was for a certain performance, I would most likely go with a different data
structure as this has O(n) for the lookup.
As there was no specification for any error handling, I chose to go silently with the invalid scenarios like adding a
game with a team that is already playing,
updating or finishing a not existing game or adding a game with a team to play against itself. Those scenarios will take
no effect and will be silently ignored.
For a more robust implementation I would most likely go with a `Either` monad returning `Left` in such scenarios with a
proper error message.
There is a space for much more constraints e.g. the score could only increment by 1, not allowing negative score etc.
For a sake of this exercise, I decided to skip that to avoid falling to a rabbit hole of inventing own requirements.

#### Tests

Tests are covering the basic scenario:

- calculate the summary table correctly
- removal of a game from the scoreboard
- removal of all games from the scoreboard which should result in an empty table
- removal of not existing game which should do nothing
- update of not existing game which should do nothing
- adding a game with a team that is already which should do nothing
- adding a game with a team to play against itself which should do nothing

### Usage

Example usage can be found in a `main` method [here](src/main/scala/ScoreBoard.scala)

To run tests run `sbt test`
