import cats.*
import cats.data.*
import cats.implicits._

import scala.collection.immutable.ListMap

type Team = String

case class Score(home: Int, away: Int):
  lazy val sum = home + away
object Score:
  val empty = Score(0, 0)

case class PlayingTeams(home: Team, away: Team):
  lazy val isPlaying: Team => Boolean = team => team == home || team == away
  lazy val isAnyPlaying: PlayingTeams => Boolean = playingTeams =>
    isPlaying(playingTeams.home) || isPlaying(playingTeams.away)

type Game = (PlayingTeams, Score)

given showGame: Show[Game] = Show.show { game =>
  s"${game._1.home} ${game._2.home} - ${game._1.away} ${game._2.away}"
}

given Ordering[(PlayingTeams, Score)] = Ordering.fromLessThan((ps1, ps2) => ps1._2.sum >= ps2._2.sum)

case class ScoreBoardState(games: ListMap[PlayingTeams, Score])
object ScoreBoardState:
  val initial = ScoreBoardState(ListMap())

type ScoreBoardOp[A] = State[ScoreBoardState, A]

object ScoreBoard:
  def startGame(playingTeams: PlayingTeams): ScoreBoardOp[Unit] = State.modify[ScoreBoardState] { s =>
    if playingTeams.away == playingTeams.home then s
    else if s.games.exists(_._1.isAnyPlaying(playingTeams)) then s
    else s.copy(s.games + (playingTeams -> Score.empty))
  }
  def updateScore(playingTeams: PlayingTeams, score: Score): ScoreBoardOp[Unit] = State.modify[ScoreBoardState] { s =>
    s.copy(s.games.updatedWith(playingTeams) {
      case Some(_) => Some(score)
      case None    => None
    })
  }
  def finishGame(playingTeams: PlayingTeams): ScoreBoardOp[Unit] = State.modify[ScoreBoardState] { s =>
    s.copy(s.games.removed(playingTeams))
  }
  def getSummary(): ScoreBoardOp[List[(PlayingTeams, Score)]] = State.inspect(s => s.games.toList.sorted)

@main def hello: Unit =
  import ScoreBoard._
  val (_, result) = (for {
    _  <- State.get[ScoreBoardState]                       // Gets us into the state monad context
    _  <- startGame(PlayingTeams("Slovakia", "Czechia"))   // Starts game 1
    _  <- startGame(PlayingTeams("Poland", "Netherlands")) // Starts game 2
    s1 <- getSummary()                                     // Gets summary, this should be 0:0 for both games
    _ <- updateScore(PlayingTeams("Slovakia", "Czechia"), Score(1, 0))   // Updates score for game 1
    _ <- updateScore(PlayingTeams("Poland", "Netherlands"), Score(0, 1)) // Updates score for game 2
    s2 <- getSummary() // Gets summary, this should be updated according previous steps, game 2 should be on top
    _ <- updateScore(PlayingTeams("Slovakia", "Czechia"), Score(3, 1))   // Updates score for game 1
    _ <- updateScore(PlayingTeams("Poland", "Netherlands"), Score(2, 1)) // Updates score for game 2
    s3 <- getSummary() // Gets summary, this should be updated according previous steps, the game 1 should be on top now
    _ <- finishGame(PlayingTeams("Slovakia", "Czechia"))   // Finishes game 1
    _ <- finishGame(PlayingTeams("Poland", "Netherlands")) // Finishes game 2
    s4 <- getSummary() // Gets summary, this should be now empty as all games were finished
  } yield List(s1, s2, s3, s4)).run(ScoreBoardState.initial).value

  result
    .map(summary => {
      summary.zipWithIndex.map((game, idx) => s"${idx + 1} ${game.show}")
    })
    //Now follows the only side effect part of the whole thing, I could have handled it with an IO monad, but my wife prefers me to clean the bathroom,
    // I think I should rather take her advise.
    .foreach(s =>
      s.foreach(println)
      println
    )
