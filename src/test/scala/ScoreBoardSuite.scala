import cats.data.State
import org.scalatest.flatspec.AnyFlatSpec
import cats.implicits.*

import scala.collection.SortedMap
import scala.collection.immutable.ListMap

class ScoreBoardSuite extends AnyFlatSpec:

  val input = List(
    (PlayingTeams("Mexico", "Canada"), Score(0, 5)),
    (PlayingTeams("Spain", "Brazil"), Score(10, 2)),
    (PlayingTeams("Germany", "France"), Score(2, 2)),
    (PlayingTeams("Uruguay", "Italy"), Score(6, 6)),
    (PlayingTeams("Argentina", "Australia"), Score(3, 1))
  )

  val ops = input
    .flatMap((players, score) =>
      List(
        ScoreBoard.startGame(players),
        ScoreBoard.updateScore(players, score)
      )
    )
    .sequence

  it should "calculate score correctly" in {
    val expected: List[(PlayingTeams, Score)] = List(
      (PlayingTeams("Uruguay", "Italy"), Score(6, 6)),
      (PlayingTeams("Spain", "Brazil"), Score(10, 2)),
      (PlayingTeams("Mexico", "Canada"), Score(0, 5)),
      (PlayingTeams("Argentina", "Australia"), Score(3, 1)),
      (PlayingTeams("Germany", "France"), Score(2, 2))
    )

    val actual = (for {
      _       <- State.get[ScoreBoardState]
      _       <- ops
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2
    assert(actual === expected)
  }

  it should "remove game from scoreboard when finished" in {
    val expected: List[(PlayingTeams, Score)] = List(
      (PlayingTeams("Spain", "Brazil"), Score(10, 2)),
      (PlayingTeams("Mexico", "Canada"), Score(0, 5)),
      (PlayingTeams("Argentina", "Australia"), Score(3, 1)),
      (PlayingTeams("Germany", "France"), Score(2, 2))
    )

    val actual = (for {
      _       <- State.get[ScoreBoardState]
      _       <- ops
      _       <- ScoreBoard.finishGame(PlayingTeams("Uruguay", "Italy"))
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2

    assert(actual === expected)
  }

  it should "remove all games from scoreboard when finished" in {
    val actual = (for {
      _       <- State.get[ScoreBoardState]
      _       <- ops
      _       <- input.map((players, _) => ScoreBoard.finishGame(players)).sequence
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2

    assert(actual.isEmpty)
  }

  it should "not remove non existing game" in {
    val actual = (for {
      _       <- State.get[ScoreBoardState]
      _       <- ops
      _       <- ScoreBoard.finishGame(PlayingTeams("Pakistan", "Lesotho"))
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2

    assert(actual.size === input.size)
  }

  it should "do nothing when updating non existing game" in {
    val actual = (for {
      _       <- State.get[ScoreBoardState]
      _       <- ops
      _       <- ScoreBoard.updateScore(PlayingTeams("Pakistan", "Lesotho"), Score(16, 12))
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2

    assert(actual.size === input.size)
  }

  it should "not add team twice if it's already playing" in {
    val actual = (for {
      _ <- State.get[ScoreBoardState]
      _ <- ScoreBoard.startGame(PlayingTeams("Germany", "Uruguay"))
      _ <- ScoreBoard.startGame(PlayingTeams("Germany", "Slovakia"))
      _ <- ScoreBoard.startGame(PlayingTeams("Poland", "Germany"))
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2

    assert(actual.size === 1)
  }

  it should "not allow team to play against itself" in {
    val actual = (for {
      _ <- State.get[ScoreBoardState]
      _ <- ScoreBoard.startGame(PlayingTeams("Germany", "Germany"))
      summary <- ScoreBoard.getSummary()
    } yield summary).run(ScoreBoardState.initial).value._2
  
    assert(actual.size === 0)
  }  